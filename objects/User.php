<?php
namespace Valarep\objects;

use Valarep\dao\UserDao;

class User
{
    public $id;
    public $login;
    public $password;

    public static function get($login, $password)
    {
        return UserDao::get($login, $password);
    }
}