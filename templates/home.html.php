<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <h1>Home</h1>
<?php if($error): ?>
        <p><?= $errorMessage; ?></p>
<?php endif; ?>
<?php 
if($connected) {
    require "disconnection_form.html.php";
} else {
    require "connection_form.html.php";
}
?>
    </body>
</html>