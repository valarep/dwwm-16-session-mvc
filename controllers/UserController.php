<?php
namespace Valarep\controllers;

use Valarep\Route;
use Valarep\Router;
use Valarep\Session;
use Valarep\View;
use Valarep\objects\User;

class UserController
{
    public static function route()
    {
        // Routage secondaire
        $router = new Router();
        $router->addRoute(new Route("/", "UserController", "homeAction"));
        $router->addRoute(new Route("/user/connect", "UserController", "connectAction"));
        $router->addRoute(new Route("/user/disconnect", "UserController", "disconnectAction"));

        $route = $router->findRoute();

        if ($route)
        {
            $route->execute();
        }
        else
        {
            // Erreur 404
            echo "Page not found";
        }
    }

    public static function homeAction()
    {
        $connected = Session::get("connected");
        $error = Session::get("error");
        $errorMessage = Session::get("errorMessage");
        $user = Session::get("user");

        //echo "homeAction works!";
        View::setTemplate("home");

        View::bindVariable("connected", $connected);
        View::bindVariable("error", $error);
        View::bindVariable("errorMessage", $errorMessage);
        View::bindVariable("user", $user);

        View::display();
    }

    public static function connectAction()
    {
        //echo "connectAction works!";

        // récupération des données du formulaire
        $login = filter_input(INPUT_POST, "login", FILTER_SANITIZE_STRING);
        $password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);

        $user = User::get($login, $password);

        if ($user)
        {
            // tout va bien
            // session valide
            Session::set('user', $user);
            Session::set('connected', true);
        }
        else
        {
            // erreur de login ou de password
            Session::set('error', true);      
            Session::set('errorMessage', "Login or password error !");      
        }

        $router = new Router();
        $path = $router->getBasePath();

        header("location: {$path}/");
    }

    public static function disconnectAction()
    {
        //echo "disconnectAction works!";
        Session::unset();

        $router = new Router();
        $path = $router->getBasePath();

        header("location: {$path}/");
    }
}