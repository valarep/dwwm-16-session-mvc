<?php
namespace Valarep;

use Exception;

class Session
{
    public static function start()
    {
        session_start();
        self::init();
    }

    private static function init()
    {
        // valeur(s) par défaut de la session
        if (empty($_SESSION))
        {
            $_SESSION['connected'] = false;
            $_SESSION['error'] = false;
            $_SESSION['errorMessage'] = "";
            $_SESSION['user'] = (object)[];
        }
    }

    public static function unset()
    {
        session_unset();
        self::init();
    }

    public static function get($name)
    {
        if (!isset($_SESSION[$name]))
        {
            throw new Exception("La variable de session $name n'est pas définie");
        }
        return $_SESSION[$name];
    }

    public static function set($name, $value)
    {
        $_SESSION[$name] = $value;
    }
}