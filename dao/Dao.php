<?php

namespace Valarep\dao;

use \PDO;
use \PDOException;
use Valarep\View;

class Dao
{
    private static $host = "127.0.0.1";
    private static $port = "3306";
    private static $database = "session";
    private static $charset = "UTF8";
    private static $user = "session";
    private static $password = "session";
    private static $connection;

    public static function open()
    {
        $dsn = "mysql:" .
                "host=" . self::$host . ";" .
                "port=" . self::$port . ";" .
                "dbname=" . self::$database . ";" . 
                "charset=" . self::$charset . ";";
        
        try {
            self::$connection = new PDO($dsn, self::$user, self::$password);
            return self::$connection;
        } catch (PDOException $ex) {
            View::setTemplate("fatal_error");

            View::bindVariable("title", "");
            View::bindVariable("code", "1");
            View::bindVariable("debugMode", false);
            View::bindVariable("ex", $ex);

            View::display();
            die();
        }
         
    }

    public static function close()
    {
        self::$connection = null;
    }
}