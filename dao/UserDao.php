<?php
namespace Valarep\dao;

use \PDO;
use \Exception;

class UserDao
{
    /**
     * get User from database
     * @param $login : user login
     * @param $password : user password
     * @return returns User if login, password matches, or null 
     */
    public static function get($login, $password)
    {
        $dbh = Dao::open();

        $query = "SELECT * 
                    FROM `user` 
                    WHERE `login` = :login 
                    AND `password` = MD5(:password);";
        
        $sth = $dbh->prepare($query);

        $sth->bindParam(":login", $login);
        $sth->bindParam(":password", $password);

        $res = $sth->execute();
        if (! $res)
        {
            // debug
            $error = $sth->errorInfo();
            die($error[2]);
        }

        if ($sth->rowCount())
        {
            // connexion réussie
            // User
            $sth->setFetchMode(
                PDO::FETCH_CLASS,
                "Valarep\\objects\\User"
            );
            $item = $sth->fetch();
        }
        else
        {
            // connexion échouée
            // null
            $item = null;
        }
        Dao::close();

        return $item;
   } 
}