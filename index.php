<?php
namespace Valarep;

// début de l'application web

// Chargement automatique des classes
require_once "vendor/autoload.php";

// initialisation des sessions
Session::start();

// Routage principal
$router = new Router();
$router->addRoute(new Route("/", "UserController"));
$router->addRoute(new Route("/user/{*}", "UserController"));

$route = $router->findRoute();

if ($route)
{
    $route->execute();
}
else
{
    // Erreur 404
    echo "Page not found";
}